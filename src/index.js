import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class PomodoroClock extends React.Component{
    constructor(props){
        super(props);
        this.state={
            breaklength: 5,
            sessionlength: 25,
            minute: 25,
            seconds: 0,
            time: '25:00',
            status: true,
            timer_label: 'Session'
        }
        this.handle_Decrement_Increment = this.handle_Decrement_Increment.bind(this);
        this.myTimer = this.myTimer.bind(this);
        this.refresh = this.refresh.bind(this);
    }
    handle_Decrement_Increment(e){
        if(this.state.status !== false){
            if(e.target.id === "break-decrement" && this.state.breaklength !== 1){
                this.setState({
                    breaklength: this.state.breaklength-1
                });
            }
            if(e.target.id === "break-increment" && this.state.breaklength !== 60){
                this.setState({
                    breaklength: this.state.breaklength+1
                });
            }
            if(e.target.id === "session-decrement" && this.state.sessionlength !== 1){
                let sessionlength = this.state.sessionlength-1;
                this.setState({
                    sessionlength: sessionlength,
                    minute: sessionlength,
                    seconds: 0,
                    time: '0'+sessionlength+':00'
                }); 
            }
            if(e.target.id === "session-increment" && this.state.sessionlength !== 60){
                let sessionlength = this.state.sessionlength+1;
                this.setState({
                    sessionlength: sessionlength,
                    minute: sessionlength,
                    seconds: 0,
                    time: '0'+sessionlength+':00'
                });   
            }
        }
    }
    myTimer(){ 
        let ss = this.state.seconds;
        let mm = this.state.minute;
        this.setState(state => {
            if (state.status === false) {
              clearInterval(this.timer);
            } else {
              this.timer = setInterval(() => {
                mm = parseInt(mm);
                ss= parseInt(ss);
                let sound = document.getElementById('beep');
                if(mm === 0 && ss === 0 && this.state.timer_label === 'Session'){
                    document.getElementById('timer-label').innerHTML = 'Break Time'
                    sound.play();
                    mm = this.state.breaklength;
                    this.setState({
                        timer_label: 'Break Time'
                    })
                }
                if(mm === 0 && ss === 0 && this.state.timer_label === 'Break Time'){
                    document.getElementById('timer-label').innerHTML = 'Session'
                    sound.play();
                    mm = this.state.breaklength;
                    this.setState({
                        timer_label: 'Session'
                    })
                }
                if(ss === 0 && mm !== 0){
                      ss = 60;
                      mm = mm-1;
                }
                ss = ss-1;
                if(ss<10){
                    ss = '0'+ss
                }
                  if(mm<10){
                    mm = '0'+mm
                }
                this.setState({ 
                  minute: mm,
                  seconds: ss,
                  time: mm+':'+ss
                });
                
                
              }, 1000);
            }
            return { status: !state.status };
          });
        
    }
    refresh(){
        this.setState({
            breaklength: 5,
            sessionlength: 25,
            minute: 25,
            seconds: 0,
            time: '25:00',
            status: true,
            timer_label: 'Session'
        })
        clearInterval(this.timer);
        document.getElementById('timer-label').innerHTML = 'Session';
        let sound = document.getElementById('beep');
        sound.pause();
        sound.currentTime = 0;
    }
    render(){
        return(
            <div>
                <div className="main-title">Pomodoro Clock</div>
                <div id="break-session-section">
                    <div id="break-container">
                        <p id="break-label">Break Length</p>
                        <div>
                            <i id="break-decrement" className="fas fa-arrow-down" onClick={this.handle_Decrement_Increment}></i>
                            <span id="break-length">{this.state.breaklength}</span>
                            <i id="break-increment" className="fas fa-arrow-up" onClick={this.handle_Decrement_Increment}></i>

                        </div>
                    </div>
                    <div id="session-container">
                    <p id="session-label">Session Length</p>
                    <div>
                        <i id="session-decrement" className="fas fa-arrow-down" onClick={this.handle_Decrement_Increment}></i>
                        <span id="session-length">{this.state.sessionlength}</span>
                        <i id="session-increment" className="fas fa-arrow-up" onClick={this.handle_Decrement_Increment}></i>
                    </div>
                </div>
                </div>
                <div id="timer-container">
                    <div id="timer">
                        <div id="timer-wrapper">
                            <p id="timer-label">Session</p>
                            <div id="time-left">{this.state.time}</div>
                        </div>
                    </div>
                    <div id="time-control">
                        <button id="start_stop" onClick={this.myTimer}>
                            <i className="fa fa-play fa-2x"></i>
                            <i className="fa fa-pause fa-2x"></i>
                        </button>
                        <button id="reset" onClick={this.refresh}>
                            <i className="fas fa-sync-alt fa-2x"></i>
                        </button>
                    </div>
                </div>
                <audio id="beep" src="https://goo.gl/65cBl1"></audio>
            </div >
        )
    }
}
ReactDOM.render(<PomodoroClock/>, document.getElementById('root'));
